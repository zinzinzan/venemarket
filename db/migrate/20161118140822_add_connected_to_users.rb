class AddConnectedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :connected, :boolean
  end
end
