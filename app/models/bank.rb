class Bank < ApplicationRecord

  has_many :accounts

  validates :bankname, presence: true,
            length: {maximum: 100}

end
