FactoryGirl.define do
  factory :pay_order do
    amountIn 1
    amountOut 1
    account nil
  end
end
