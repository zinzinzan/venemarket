class AddRateRefToPayOrders < ActiveRecord::Migration[5.0]
  def change
    add_reference :pay_orders, :rate, foreign_key: true
  end
end
