class Account < ApplicationRecord
  belongs_to :bank
  belongs_to :user

  has_many :pay_orders

  validates :holdername, presence: true

  validates :number, presence: true, length: {is: 20}

  validates :documentid, presence: true

  validates :bank_id, presence: true

  validates :user_id, presence: true

  def name_with_bank
    "#{bank.bankname} - #{holdername} - #{number}"
  end

end
