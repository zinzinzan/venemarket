class TransactionTypesController < ApplicationController

  before_action :sing_in_user

  def new
    #   @transaction_type= TransactionType.new
  end

  def create
    #@transaction_type = TransactionType.create(transaction_type_params)
  end

  def index
    #TODO filtrar por usuario
    current_user.accounts.each { |account|
      @pay_orders = account.pay_orders.each { |pay_order|
        @transaction_types = pay_order.transaction_type }
    }
  end

  def transaction_type_params
    params.require(:transaction_type).permit(:value)
  end

end
