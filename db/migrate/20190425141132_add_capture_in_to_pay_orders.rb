class AddCaptureInToPayOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :pay_orders, :capture_in, :string
  end
end
