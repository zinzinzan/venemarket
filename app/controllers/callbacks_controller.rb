class CallbacksController < ApplicationController

  def facebook
    @user = User.from_omniauth(request.env["omniauth.auth"])
    if @user.persisted?
      flash[:notice] = 'Has ingresado con tu cuenta Facebook'
      #TODO mejorar se esta creando connectar user siempre y solo se debe crear la primera vez y luego actualizar el connected true
      if (ConnectedUser.find_by(user_id: @user.id)== nil)
        connected_user=ConnectedUser.new
        connected_user.user=@user
        connected_user.user.connected=true
        #TODO check if this is needed
        connected_user.user.save
        connected_user.save
      end
      sign_in_and_redirect @user, event: :authentication
    else

      session["devise.facebook_data"] = request.env["omniauth.auth"].except(:extra)
      redirect_to new_user_registration_url, alert: @user.errors.full_messages.join("\n")

    end


  end

  def google_oauth2
    @user = User.from_omniauth(request.env["omniauth.auth"])

    # add_to_connected_user user
    if @user.persisted?
      #flash[:notice] = I18n.t 'devise.omniauth_callbacks.success', kind: 'Google'
      flash[:notice] = 'Has ingresado con tu cuenta Google'
      #TODO mejorar se esta creando connectar user siempre y solo se debe crear la primera vez y luego actualizar el connected true
      if (ConnectedUser.find_by(user_id: @user.id)== nil)
        connected_user=ConnectedUser.new
        connected_user.user=@user
        connected_user.user.connected=true
        #TODO check if this is needed
        connected_user.user.save
        connected_user.save
      end

      sign_in_and_redirect @user, event: :authentication
    else
      session['devise.google_data'] = request.env['omniauth.auth'].except(:extra) # Removing extra as it can overflow some session stores
      redirect_to new_user_registration_url, alert: @user.errors.full_messages.join("\n")
    end

    #redirect_to controller: 'static_pages', action: 'home', chat: 1
    # redirect_to controller: 'static_pages', action: 'home'
  end

  def failure
    flash.now[:danger] = 'Invalid username/password combination'
    redirect_to root_url
  end

end


