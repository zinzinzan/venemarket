class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :holdername
      t.string :number
      t.string :documentid
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
