class StaticPagesController < ApplicationController

  def home
    @connected_users = ConnectedUser.all
    if user_signed_in?
      @aux_user = nil

      @connected_users.each_with_index do |connected_user, index|
           if connected_user.user.id ==
                  current_user.id
              @aux_user= connected_user
             return
           end
      end

      if @aux_user == nil
       connected_user=ConnectedUser.new
       connected_user.user=current_user
       connected_user.user.connected=true
       #TODO check if this is needed
       connected_user.user.save
       connected_user.save
       #TODO improve this soo we dont need to hit database
       @connected_users = ConnectedUser.all
      end

    end

  end

  def use_conditions

  end

end
