require 'rails_helper'

RSpec.describe Rate, type: :model do

  let(:rate) { @rate= Rate.create(value: 3000) }
  subject { rate }


  context 'valid' do

    it { should respond_to(:value) }
    it { should be_valid }

    context 'Asociations' do
      it { should have_many(:pay_orders) }
    end
  end

  context 'invalid' do

    describe 'when valueis not present' do
      before { rate.value = '' }
      it { should_not be_valid }
    end

    describe 'when  value is not a digit' do
      before { rate.value = '1a' }
      it { should_not be_valid }
    end

  end

end
