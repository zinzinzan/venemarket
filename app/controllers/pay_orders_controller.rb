class PayOrdersController < Wicked::WizardController

  steps :show_rate,:last_step, :confirm_data

  before_action :sing_in_user
  before_action :save_previous_url

  def new
    if current_user.accounts.all.length == 0
      flash[:warning] = 'Debes agregar primero una bancaria cuenta de Venezuela'
      redirect_to controller: 'accounts', action: 'new'
    else
      @pay_order = PayOrder.new
      @account = Account.new
    end
  end

  def create
    @pay_order = PayOrder.new(pay_oder_params)
    @pay_order.rate = current_rate
    @pay_order.user = current_user
    @pay_order.transaction_type = TransactionType.find_by(status: "Pendiente por Pagar")
    if @pay_order.save
      flash[:danger] = @pay_order.capture_in.current_path
      flash[:success] = 'Orden de pago ha sido creada exitosamente'
      redirect_to action: 'index'
    else
      render 'new'
    end
  end

  def index
    @pay_orders = current_user.pay_orders.paginate(page: params[:page])
  end

  def show
    @pay_order = PayOrder.find_by_id(params[:id])
  end

  def update
    if step == :show_rate
      params[:pay_order][:account_id] = '1'
      @pay_order = PayOrder.find_by_id()
      @pay_order.rate = current_rate
      @pay_order.user = current_user
      @pay_order.transaction_type = TransactionType.find_by(status: "Pendiente por Pagar")
    end
    params[:pay_order][:current_step] = step
    @pay_order.update_attributes(pay_oder_params)
    render_wizard @pay_order
    #end
  end

  def save_previous_url
    session[:my_previous_url] = URI(request.referer || '').path
  end


  def pay_oder_params

    params.require(:pay_order).permit(:amount_in, :amount_out, :account_id, :capture_in, :capture_in_cache,
                                      :current_step)
  end



end