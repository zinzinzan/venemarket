class CreatePayOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :pay_orders do |t|
      t.integer :amount_in
      t.integer :amount_out
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
