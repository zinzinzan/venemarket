require 'rails_helper'
require 'spec_helper'

RSpec.describe PayOrder, type: :model do

  let!(:user) { @user = User.create(username: 'Example user', email: 'user@example.com', password: 'foobar') }
  let(:account) { @account = Account.new(holdername: 'rafael', number: '11111111111111111111',
                                         documentid: '18208301') }
  let(:bank) { @bank = Bank.create(bankname: "banco de la mujer") }
  let(:pay_order) { @pay_order = PayOrder.new(amount_in: 100, amount_out: 3500000) }
  let(:rate) { @rate= Rate.create(value: 3000) }
  let(:transaction_type) { @transaction_type = TransactionType.create(status: "Pendiente") }

  subject { pay_order }

  before {
    account.bank = bank
    account.user = user
    account.save
    pay_order.account = account
    pay_order.rate = rate
    pay_order.transaction_type = transaction_type
    pay_order.user = user
    pay_order.save
  }

  context 'valid' do
    it { should respond_to(:amount_in) }
    it { should respond_to(:amount_out) }

    it { should be_valid }

    context 'Asociations' do
      it { should belong_to(:rate) }
      it { should belong_to(:account) }
      it { should belong_to(:transaction_type) }
      it { should belong_to(:user) }
    end

  end

  context 'invalid' do

    describe 'when amount in is not present' do
      before { pay_order.amount_in = '' }
      it { should_not be_valid }
    end

    describe 'when  amount_in is not a digit' do
      before { pay_order.amount_in = '1a' }
      it { should_not be_valid }
    end

    describe 'when amount_out is not present' do
      before { pay_order.amount_out= '' }
      it { should_not be_valid }
    end

    describe 'when amount_out is not a digit' do
      before { pay_order.amount_out= '1' }
      it { should_not be_valid }
    end


    describe 'when account ref is not present' do
      before { pay_order.account_id = nil }
      it { should_not be_valid }
    end

    describe 'when transtaction_type ref is not present' do
      before { pay_order.transaction_type_id = nil }
      it { should_not be_valid }
    end

    describe 'when user_id is not present' do
      before { pay_order.user_id = nil }
      it { should_not be_valid }
    end

  end

end