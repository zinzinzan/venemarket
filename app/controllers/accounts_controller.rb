class AccountsController < ApplicationController

  before_action :sing_in_user

  def new
    @account = Account.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @account = Account.new(account_params)
    @account.user= current_user
    if @account.save
      flash[:success]="Cuenta creada exitosamente"
      if request.referrer == 'http://localhost:3000/pay_orders/last_step'
        redirect_to request.referrer
      else
        redirect_to root_path
      end
    else
      msg = ''
      if params[:account][:bank_id].blank?
        msg = 'Debes seleccionar un banco, cuando crees la cuenta'
      else
        msg= 'Ha habido un error al crear la cuenta'
      end
      flash[:warning]= msg
      if request.referrer == new_pay_order_url
        redirect_to new_pay_order_path
      else
        render 'new'
      end

    end

  end

  # t.integer  "user_id"
  def account_params
    params.require(:account).permit(:holdername, :number, :documentid, :bank_id)
  end

  def index
    @accounts = current_user.accounts
  end

  def edit
    #@accounts = current_user.accounts
  end


end
