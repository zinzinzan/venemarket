class PayOrder < ApplicationRecord

  mount_uploader :capture_in, CaptureInUploader

  belongs_to :account
  belongs_to :rate
  belongs_to :transaction_type
  belongs_to :user
  accepts_nested_attributes_for :account

  attr_accessor :current_step

  validates :amount_out, presence: true, numericality: { only_float: true}
  validates :amount_in, presence: true, numericality: {only_float: true}
  validates :account_id, presence: true
  validates :transaction_type_id, presence: true
  validates :user_id, presence: true
  validates :capture_in, presence: true
  validate  :picture_size

 #validates the size of an upload image
  def picture_size
    if capture_in.size > 5.megabytes
      errors.add(:capture_in,"debe ser menor a 5MB.")
    end
  end

  def current_step?(step_key)
    #should return false
    current_step.blank? || current_step == step_key
  end


end
