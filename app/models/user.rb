class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]

  has_many :messages
  has_many :pay_orders
  has_many :accounts

  validates :username, presence: true,
            length: {maximum: 50}
=begin

  validates :password, presence: true, length: { minimum: 6 }
  has_secure_password
=end
  #devise  concerns validates the rest
  validates :email, length: {maximum:100 }

  def self.from_omniauth(auth)

    logger.debug "name" + " " + auth.info.name
    logger.debug "prov" + " " +  auth.provider
    logger.debug "uid" + " " +  auth.uid
    logger.debug "email" +  " " + auth.info.email
    #TODO in DB first_or_create  if no user found then create one
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.token = auth.credentials.token
      user.expires = auth.credentials.expires
      user.expires_at = auth.credentials.expires_at
      user.refresh_token = auth.credentials.refresh_token
      user.email = auth.info.email
      user.username = auth.info.name
      user.password = Devise.friendly_token[0,20]
    end
    #TODO catch the BD exception and show it to the user

  end

end
