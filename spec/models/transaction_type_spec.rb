require 'rails_helper'
require 'spec_helper'

RSpec.describe TransactionType, type: :model do

  let(:transaction_type) { @transaction_type = TransactionType.create(status: "Pendiente") }

  subject { transaction_type }


  context 'valid' do
    it { should respond_to(:status) }
    it { should be_valid }

    context 'Asociations' do
      it { should have_many(:pay_orders) }
    end

  end

  context 'invalid' do

    describe 'when status in is not present' do
      before { transaction_type.status= '' }
      it { should_not be_valid }
    end

  end
end
