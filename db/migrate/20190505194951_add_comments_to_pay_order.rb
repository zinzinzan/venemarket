class AddCommentsToPayOrder < ActiveRecord::Migration[5.0]
  def change
    add_column :pay_orders, :comment, :string
  end
end
