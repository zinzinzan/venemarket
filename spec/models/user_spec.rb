require 'rails_helper'
require 'spec_helper'

RSpec.describe User, type: :model do

  before { @user = User.new(username: 'Example user', email: 'user@example.com', password: 'foobar')}

  subject { @user }

  context 'valid' do
    it { should respond_to(:username) }
    it { should respond_to(:email) }

    it { should be_valid }
    context 'Associations' do
      it { should have_many(:messages) }
      it { should have_many(:accounts) }
      it { should have_many(:pay_orders) }

    end

  end

  context 'invalid' do

    describe 'when name is not present' do
      before { @user.username= '' }
      it { should_not be_valid }
    end

    describe 'when name is blank' do
      before { @user.username= '    ' }
      it { should_not be_valid }
    end


    describe 'when email is no present' do
      before {@user.email=''}
      it {should_not be_valid}
    end

    describe 'when passward is not present' do
      before {@user.password=''}
      it {should_not be_valid}
    end

  end

end