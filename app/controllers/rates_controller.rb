class RatesController < ApplicationController

  #before_action :current_user_admin
  before_action :sing_in_user

  def new
    if current_user_admin?
      @rate = Rate.new
    else
      flash[:danger] = 'Solo el usuario administrado puede acceder '
      redirect_to root_path
    end

  end

  def create
    @rate = Rate.new(rate_params)
    if @rate.save
      flash.now[:success] = 'Orden de pago ha sido creada exitosamente'
      redirect_to root_path
    else
      flash.now[:danger] = 'ha habido un error'
      render 'new'
    end
  end

  def rate_params
    params.require(:rate).permit(:value)
  end

end
