class AddTransactionToPayOrders < ActiveRecord::Migration[5.0]
  def change
    add_reference :pay_orders, :transaction_type, foreign_key: true
  end
end
