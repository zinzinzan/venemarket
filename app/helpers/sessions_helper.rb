module SessionsHelper
=begin
  # Logs in the given user.
  def log_in(user)
    cookies.signed[:user_id] = user.id
    if (ConnectedUser.find_by(user_id: user.id)== nil)
      connected_user=ConnectedUser.new
      connected_user.user=user
      connected_user.user.connected=true
      connected_user.user.save
      connected_user.save
    end

  end

  def log_out
    connected_user = ConnectedUser.find_by(user_id: current_user.id)
    if (connected_user != nil)
      connected_user.user.connected=false
      connected_user.user.save
      connected_user.destroy
    end

    cookies.delete(:user_id)
    @current_user = nil

  end

  # Returns the current logged-in user (if any).
  def current_user
    @current_user ||= User.find_by(id: cookies.signed[:user_id])
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end
=end
end