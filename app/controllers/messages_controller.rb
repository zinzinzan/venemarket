class MessagesController < ApplicationController

  before_action :sing_up_user
  before_action :get_messages

  def index
  end

  #respond_to :js

  def create
    #@path = conversation_path(@conversation)
    #render :format => :js

    @conversation = Conversation.find(params[:conversation_id])

    message = @conversation.messages.build(message_params)
    @message = message
    if message.save
=begin
      ActionCable.server.broadcast 'room_channel',
                                   message: render_message(message)
=end

      ActionCable.server.broadcast 'room_channel',
                                   {message: render_message(message), conversation_id: @conversation.id,
                                    message_user_id: @message.user.id}

      ActionCable.server.broadcast "room_channel_user_#{recipient_id(@conversation, message)}",
                                   {title: @message.user.username, body: message.content, notification: true}

    end
  end

  private

  def get_messages
    @messages = Message.for_display
    @message = current_user.messages.build
  end

=begin

  def message_params
    params.require(:message).permit(:content, :tgt_user_id)
  end
=end

  def message_params
    params.permit(:content, :user_id, :conversation_id)
  end

  def render_message (message)
    #same thing
    #render(partial: 'message', locals: {message: message})
    render(partial: message)
  end

  def recipient_id (conversation, message)
    conversation.sender.id == message.user.id ? conversation.recipient.id : conversation.sender.id
  end

end