module UsersHelper

  def get_admin
    User.find_by(admin: true)
  end

  def current_user_admin?
    if current_user.admin == true
      return true
    else
      return false
    end
  end



end
