class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  #before_action :authenticate_user!
  include Devise::Controllers::Helpers
  include RatesHelper
  include UsersHelper


  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

  # Confirms a logged-in user.
  def sing_up_user
    unless user_signed_in?
      flash[:danger] = "Registrate mediante Google, Facebook o crear una cuenta"
      redirect_to new_user_registration_path
    end
  end

  def sing_in_user
    unless user_signed_in?
      flash[:danger] = "Ingresa mediante tu cuenta Google, Facebook o tu usuario y clave"
      redirect_to new_user_session_path
    end
  end

end