Rails.application.routes.draw do
  get 'accounts/new'

  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  #devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'

  resources :users
  resources :messages
  resources :accounts
  resources :pay_orders
  resources :rates
  resources :transaction_types

  #get "posts/new" => 'posts#new_post', :as => :post

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  mount ActionCable.server, at: '/cable'

  get '/users/conversations/:id' => 'conversations#show'

  resources :conversations do
    resources :messages
  end

 get '/auth/:provider/callback', to: 'sessions#create'


end
