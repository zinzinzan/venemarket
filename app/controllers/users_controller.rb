=begin

class UsersController < ApplicationController

  layout false
  def new
    @user = User.new
    if (params[:from_chat] == 'true')
      flash.now[:danger] = 'Debes logearte mediante tu cuenta Google o Facebook, ' +#
          'tambien puedes crear un usuario  para poder comunicarte con nostros'
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      admin = User.find_by(username: 'venemarket')
      @conversation = Conversation.create!(sender_id: admin.id, recipient_id: @user.id)
      message = @conversation.messages.build(content: 'Bienvenido nuestro administrador se pondra en contacto con usted en breve', user_id: admin.id)
      #params.permit(:content, :user_id, :conversation_id)

      @message = message
      if message.save
        ActionCable.server.broadcast "room_channel_user_#{admin.id}",
                                     {title: @user.username, body: message.content, notification: true}

        redirect_to controller: 'static_pages', action: 'home', chat: 1 and return
      end
    end
    render 'new'
  end

  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation)
  end

  def show

  end
end
=end
