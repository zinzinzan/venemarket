class AddUserToPayOrders < ActiveRecord::Migration[5.0]
  def change
    add_reference :pay_orders, :user, foreign_key: true
  end
end
