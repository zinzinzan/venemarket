App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server
  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log(data)
    new Notification data.title, body: data.body if data.notification
    if (data.message && !data.message.blank?)
      $('#chatbox_' + data.conversation_id).show()
      $('.text_area_' + data.conversation_id).val ''
      $('.text_area_' + data.conversation_id).focus()
      $('.text_area_' + data.conversation_id).css 'height', '44px'

      id = data.conversation_id
      chatbox = $('#chatbox_' + id + ' .chatboxcontent')
      sender_id = data.message_user_id
      chatbox.append data.message
      chatbox.scrollTop(chatbox[0].scrollHeight)
      scroll_bottom()

$(document).on 'turbolinks:load', ->
  submit_message()
  scroll_bottom()
  $("form#new_message").submit (event) ->
    $('#messages_content').value = ""
    event.preventDefault()

submit_message = () ->
  $('#message_content').on 'keydown', (event) ->
    if(event.keyCode is 13 && !event.shiftKey )
      $('input').click()
      event.target.value = ""
      event.preventDefault()
      alert("coffe");

scroll_bottom = () ->
  $('#messages').scrollTop($('#messages')[0].scrollHeight)
