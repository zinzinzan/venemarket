require 'rails_helper'
require 'spec_helper'

RSpec.describe Account, type: :model do

  let!(:user) { @user = User.create(username: 'Example user', email: 'user@example.com', password: 'foobar') }
  let(:account) { @account = Account.new(holdername: 'rafael', number: '11111111111111111111', documentid: '18208301') }
  let(:bank) { @bank = Bank.create(bankname: "banco de la mujer") }
  subject { account }

  before { account.user = user
  account.bank = bank
  account.save
  }

  context 'valid' do
    it { should respond_to(:holdername) }
    it { should respond_to(:number) }
    it { should respond_to(:documentid) }

    it { should be_valid }

    context 'Asociations' do
      it { should belong_to(:user) }
      it { should belong_to(:bank) }
      it { should have_many(:pay_orders) }
    end

  end

  context 'invalid' do

    describe 'when bank is not present' do
      before { account.bank = nil
      account.save }
      it { should_not be_valid }
    end

    describe 'when name is not present' do
      before { account.holdername= '' }
      it { should_not be_valid }
    end

    describe 'when name is blank' do
      before { account.holdername= '    ' }
      it { should_not be_valid }
    end

    describe 'when numbe is not present' do
      before { account.number= '' }
      it { should_not be_valid }
    end

    describe 'when number is blank' do
      before { account.number= '    ' }
      it { should_not be_valid }
    end


    describe 'when number is more than 20 digits' do

      before { account.number = '1'*21 }
      it { should_not be_valid }

    end

    describe 'when number is less than 20 digits' do

      before { account.number = '1'*19 }
      it { should_not be_valid }

    end


    describe 'when document is not present' do
      before { account.documentid= '' }
      it { should_not be_valid }
    end

    describe 'when document is blank' do
      before { account.documentid= '    ' }
      it { should_not be_valid }
    end

  end

end