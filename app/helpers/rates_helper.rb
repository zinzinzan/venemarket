module RatesHelper

  def current_rate
    Rate.last
  end

  def current_rate_ves_eur
    ves_eur = 1/current_rate.value
    ves_eur.round(6)
  end

end
