class Rate < ApplicationRecord

  has_many :pay_orders
  validates :value, presence: true, numericality: {only_float: true}

end
