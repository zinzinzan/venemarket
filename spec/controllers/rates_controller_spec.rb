require 'rails_helper'
require 'spec_helper'
include UsersHelper

RSpec.describe RatesController, type: :controller do

  before { @user = User.new(username: 'Example user', email: 'user@example.com', password: 'foobar')
  #TODO need to acces in this spec to Devise::current_user and set it up

  }

  subject { @user }

  describe 'get new' do
    it 'if not admin, should redirect to login and say is only for admin' do
      get :new
      if get_admin == user
        expect(response).to have_http_status(:success)
      else
        redirect_to new_user_session_path
      end
    end
  end

end
